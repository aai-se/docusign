package com.automationanywhere.docusign;

public class DocuSignSession {
	
	private String clientID;
	private String privateKey ;
	private String accountID;
	private String userID;
	private String basePath;
	private String token;

	
	public DocuSignSession(String clientid, String privatekey, String accountid, String userid, String basepath) {
		this.clientID = clientid;
		this.accountID = accountid;
		this.userID = userid;
		this.privateKey = privatekey;
		this.basePath = basepath;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String getBasePath() {
		return this.basePath;
	}
	
	public String getAccountID() {
		return this.accountID;
	}
	
	public void setToken(String tokenValue) {
		this.token = tokenValue;
	}


}
