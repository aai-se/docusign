package com.automationanywhere.docusign;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;



import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.json.JSONObject;

import com.auth0.jwt.exceptions.JWTCreationException;


import io.fusionauth.jwt.Signer;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.rsa.RSASigner;

public class AuthHelper {
	/** live/production base path */
	  public final static String PRODUCTION_REST_BASEPATH = "https://www.docusign.net/restapi";
	  /** sandbox/demo base path */
	  public final static String DEMO_REST_BASEPATH = "https://demo.docusign.net/restapi";
	  /** stage base path */
	  public final static String STAGE_REST_BASEPATH = "https://stage.docusign.net/restapi";
	  
		// OAuth base path constants
		/** live/production base path */
		public final static String PRODUCTION_OAUTH_BASEPATH = "account.docusign.com";
		/** sandbox/demo base path */
		public final static String DEMO_OAUTH_BASEPATH = "account-d.docusign.com";
		/** stage base path */
		public final static String STAGE_OAUTH_BASEPATH = "account-s.docusign.com";
	  
	  
	  public final static String GRANT_TYPE_JWT = "urn:ietf:params:oauth:grant-type:jwt-bearer";

	  private String basePath ;
	  private String oAuthBasePath ;
	  
	 
	       
	  
	
	 public  String generateJWTAssertionFromByteArray(byte[] rsaPrivateKey, String basepath , String clientId, String userId,  List<String> scopes) throws IllegalArgumentException, JWTCreationException, IOException {
		
  	  Security.addProvider(new BouncyCastleFipsProvider());
  	 String formattedScopes = (scopes == null || scopes.size() < 1) ? "" : scopes.get(0);
     StringBuilder sb = new StringBuilder(formattedScopes);
     for (int i = 1; i < scopes.size(); i++) {
       sb.append(" " + scopes.get(i));
     }

     this.basePath = basepath;
     deriveOAuthBasePathFromRestBasePath();
	     Signer signer = RSASigner.newSHA256Signer(new String(rsaPrivateKey));
		JWT jwt = new JWT()
				.setIssuer(clientId)
				.setSubject(userId)
                .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
                .setExpiration(ZonedDateTime.now(ZoneOffset.UTC).plusMinutes(30))
                .setAudience(this.oAuthBasePath)
                .addClaim("scope",  sb.toString());
		String jwtToken = JWT.getEncoder().encode(jwt, signer);
		return jwtToken;
	}
	 
	 
	 public String getToken(String assertion) throws Exception  {
		 
		 	String authURL = "https://"+this.getOAuthBasePath() + "/oauth/token?assertion="+assertion+"&grant_type="+GRANT_TYPE_JWT;
		 	String response = POSTRequest(new URL(authURL));
		 	if (!response.contains("POST NOT WORKED")) {
		 		JSONObject oAuthObj = new JSONObject(response);
		 		return oAuthObj.getString("access_token");
		 	}
		 	else {
		 		return response;
		 	}
	 }


	
	
	  private void deriveOAuthBasePathFromRestBasePath() {
		    if (this.basePath == null) { // this case should not happen but just in case
		      this.oAuthBasePath = PRODUCTION_OAUTH_BASEPATH;
		    } else if (this.basePath.startsWith("https://demo") || this.basePath.startsWith("http://demo")) {
		      this.oAuthBasePath = DEMO_OAUTH_BASEPATH;
		    } else if (this.basePath.startsWith("https://stage") || this.basePath.startsWith("http://stage")) {
		      this.oAuthBasePath = STAGE_OAUTH_BASEPATH;
		    } else {
		      this.oAuthBasePath = PRODUCTION_OAUTH_BASEPATH;
		    }
		  }
	
	  
	  private String getOAuthBasePath() {
		    return this.oAuthBasePath;
		  }
	  
	  
	  
	  

		public static String  POSTRequest(URL posturl) throws IOException  {

			int responseCode = 0;
		    HttpURLConnection postConnection = null;


			        	postConnection = (HttpURLConnection) posturl.openConnection();
					    postConnection.setRequestMethod("POST");
				    	postConnection.setRequestProperty("Cache-Control", "no-store");
				    	postConnection.setRequestProperty("Pragma","no-cache");
				    	postConnection.setDoOutput(true);
				    	OutputStream os = postConnection.getOutputStream();
				    	os.flush();
				    	os.close();
				    	responseCode = postConnection.getResponseCode();


		    if (responseCode == HttpURLConnection.HTTP_OK) { //success
		    	
		 

		        BufferedReader in = new BufferedReader(new InputStreamReader(

		            postConnection.getInputStream(),StandardCharsets.UTF_8.name()));

		        String inputLine;

		        StringBuffer response = new StringBuffer();

		        while ((inputLine = in .readLine()) != null) {

		            response.append(inputLine);

		        } in .close();

		        
		        return  response.toString() ;

		    } else {
		    	
		        return "POST NOT WORKED "+Integer.valueOf(responseCode).toString()+" "+postConnection.getResponseMessage();

		    }

		}
		
	    
	 

		
	
}
