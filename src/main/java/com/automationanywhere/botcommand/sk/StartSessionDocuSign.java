/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.docusign.AuthHelper;
import com.automationanywhere.docusign.DocuSignSession;
import com.docusign.esign.client.auth.OAuth;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Session", name = "startDocuSignSession", description = "Start New Session", 
icon = "pkg.svg", node_label = "start session {{sessionName}}|" , comment = true ,background_color =  "#e9ddaf"  ) 
public class StartSessionDocuSign {
 
    @Sessions
    private Map<String, Object> sessions;
    
    
    
	private DocuSignSession docusignSession;
	
	
	  private static final Logger logger = LogManager.getLogger(StartSessionDocuSign.class);

    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
    				  @Idx(index = "2", type = TEXT)  @Pkg(label = "DocuSign Base URL" , default_value_type =  DataType.STRING, default_value = "https://demo.docusign.net") @NotEmpty String basePath,
			          @Idx(index = "3", type = CREDENTIAL)  @Pkg(label = "Account ID", default_value_type =  DataType.STRING) @NotEmpty SecureString accountID,
                      @Idx(index = "4", type = CREDENTIAL) @Pkg(label = "Client ID"  , default_value_type = DataType.STRING ) @NotEmpty SecureString clientID,
                      @Idx(index = "5", type = CREDENTIAL) @Pkg(label = "API User ID"  , default_value_type = DataType.STRING ) @NotEmpty SecureString userID,
			          @Idx(index = "6", type = CREDENTIAL)  @Pkg(label = "Private Key", default_value_type =  DataType.STRING, description = "Private key without pre/post boundary text") @NotEmpty SecureString privateKey
    		) throws Exception {
 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException("Session name in use") ;
       
        
        String encodedPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\r\n"+privateKey.getInsecureString().replaceAll("\n", "").replaceAll("\r", "").replaceAll("-----BEGIN RSA PRIVATE KEY-----","").replaceAll("-----END RSA PRIVATE KEY-----","")+"\r\n-----END RSA PRIVATE KEY-----";
        byte[] keyBytes =  encodedPrivateKey.getBytes();
        List<String> scopes = new ArrayList<String>() ;
        scopes.add(OAuth.Scope_SIGNATURE);
        scopes.add(OAuth.Scope_IMPERSONATION);
        
        AuthHelper authHelper = new AuthHelper();
        String assertion = authHelper.generateJWTAssertionFromByteArray(keyBytes,basePath, clientID.getInsecureString(),userID.getInsecureString(), scopes);
        String accessToken = authHelper.getToken(assertion);
        
        this.docusignSession = new DocuSignSession(clientID.getInsecureString(),encodedPrivateKey,accountID.getInsecureString(),userID.getInsecureString(),basePath);
        docusignSession.setToken(accessToken);
        
        this.sessions.put(sessionName, this.docusignSession);
    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
 
    
    
}