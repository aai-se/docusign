package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.docusign.DocuSignSession;
import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.ApiException;

@BotCommand
@CommandPkg(return_label = "Get document", node_label = "getDocument", label = "Get Document", description = "Downloads signed documents", name = "GetDocument", icon = "pkg.svg", return_type = STRING, comment = true, background_color = "#e9ddaf", return_required = true)

public class GetDocument {

	@Sessions
	private Map<String, Object> sessions;

	@Execute
	public StringValue action(
			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
			@Idx(index = "2", type = TEXT) @Pkg(label = "Envelope Id", default_value_type = STRING) @NotEmpty String envelopeId,
			@Idx(index = "3", type = TEXT) @Pkg(label = "Document Id", default_value_type = STRING, default_value = "combined") @NotEmpty String documentId,
			@Idx(index = "4", type = TEXT) @Pkg(label = "PDF file path", default_value_type = STRING) @NotEmpty String doc)
//			@Idx(index = "4", type = AttributeType.FILE) @Pkg(label = "PDF file", default_value_type = DataType.FILE) @NotEmpty String doc)
			throws Exception {

		try {
			DocuSignSession docuSignSession = (DocuSignSession) this.sessions.get(sessionName);

			// Step 2. Call DocuSign to get the document
			ApiClient apiClient = new ApiClient(docuSignSession.getBasePath() + "/restapi");

			apiClient.addDefaultHeader("Authorization", "Bearer " + docuSignSession.getToken());
			EnvelopesApi envelopesApi = new EnvelopesApi(apiClient);
			byte[] results = envelopesApi.getDocument(docuSignSession.getAccountID(), envelopeId, documentId);

			File file = new File(doc);
			OutputStream os = new FileOutputStream(file);
			// Starts writing the bytes in it
			os.write(results);

			// Close the file
			os.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new StringValue("File successfully downloaded!");
	}

	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
	}
}
