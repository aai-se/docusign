package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.docusign.DocuSignSession;
import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.model.Envelope;

@BotCommand
@CommandPkg(return_label = "Envelope status", node_label = "getEnvelopeStatus", label = "Get Envelope Status", description = "sends signing request via email for a PDF document", name = "GetEnvelopeStatus", icon = "pkg.svg", return_type = STRING, comment = true, background_color = "#e9ddaf", return_required = true)

public class GetEnvelopeStatus {

	@Sessions
	private Map<String, Object> sessions;

	@Execute
	public StringValue action(
			@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
			@Idx(index = "2", type = TEXT) @Pkg(label = "Envelope Id", default_value_type = STRING) @NotEmpty String envelopeId)
			throws Exception {

		DocuSignSession docuSignSession = (DocuSignSession) this.sessions.get(sessionName);

		// Step 2. Call DocuSign to create and send the envelope
		ApiClient apiClient = new ApiClient(docuSignSession.getBasePath() + "/restapi");

		apiClient.addDefaultHeader("Authorization", "Bearer " + docuSignSession.getToken());
		EnvelopesApi envelopesApi = new EnvelopesApi(apiClient);
		Envelope results = null;

		//
//    String envelopeId ;
		results = envelopesApi.getEnvelope(docuSignSession.getAccountID(), envelopeId);
		String status = results.getStatus();

		// Show results
		return new StringValue(status);

	}

	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
	}

}
