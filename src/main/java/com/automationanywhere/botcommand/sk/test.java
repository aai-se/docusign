package com.automationanywhere.botcommand.sk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.auth0.jwt.exceptions.JWTCreationException;
import com.automationanywhere.docusign.AuthHelper;
import com.docusign.esign.client.auth.OAuth;

public class test {

	public static void main(String[] args) throws Exception, JWTCreationException, IOException {
	
		String privatekey = "--";

        String encodedPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\r\n"+privatekey.replaceAll("\n", "").replaceAll("\r", "").replaceAll("-----BEGIN RSA PRIVATE KEY-----","").replaceAll("-----END RSA PRIVATE KEY-----","")+"\r\n-----END RSA PRIVATE KEY-----";
        byte[] keyBytes =  encodedPrivateKey.getBytes();
        List<String> scopes = new ArrayList<String>() ;
        scopes.add(OAuth.Scope_SIGNATURE);
        scopes.add(OAuth.Scope_IMPERSONATION);
        
        AuthHelper authHelper = new AuthHelper();
        String assertion = authHelper.generateJWTAssertionFromByteArray(keyBytes,"https://demo.docusign.net", "05b9052d-2058-485b-b944-2851da7580d5","3fef311b-1e9c-4b53-83cb-cb27c4e520a0", scopes);
		System.out.println(assertion);
		
		String token = authHelper.getToken(assertion);
		System.out.println(token);
        // TODO Auto-generated method stub

	}

}
