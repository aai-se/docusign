/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.sk;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.docusign.AuthHelper;
import com.automationanywhere.docusign.DocuSignSession;
import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.auth.OAuth;
import com.docusign.esign.client.auth.OAuth.OAuthToken;
import com.docusign.esign.model.Document;
import com.docusign.esign.model.EnvelopeDefinition;
import com.docusign.esign.model.EnvelopeSummary;
import com.docusign.esign.model.Recipients;
import com.docusign.esign.model.SignHere;
import com.docusign.esign.model.Signer;
import com.docusign.esign.model.Tabs;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 *
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(return_label = "envelope ID", node_label = "sendEnvelopeExcel", 



label = "Send Envelope Excel", description = "sends signing request via email for a Excel document", 
name = "sendenvelopeXLSX", icon = "pkg.svg", return_type = STRING, comment = true ,background_color =  "#e9ddaf"  ,return_required = true)
public class SendEnvelopeXLSX{
	

    @Sessions
    private Map<String, Object> sessions;
    
    
	private static final Logger logger = LogManager.getLogger(SendEnvelopeXLSX.class);
    
	@Execute
	   public StringValue action(
			    @Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName ,
				@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Excel file" , default_value_type = DataType.FILE) @NotEmpty String doc ,
	            @Idx(index = "3", type = TEXT) @Pkg(label = "Signer Full Name" , default_value_type =  DataType.STRING) @NotEmpty String signerName,
	            @Idx(index = "4", type = TEXT) @Pkg(label = "Signer eMail" , default_value_type = DataType.STRING) @NotEmpty String signerEmail,
                @Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Page" , default_value_type = DataType.NUMBER) @NotEmpty Number page,
				@Idx(index = "6", type = TEXT) @Pkg(label = "Anchor" , default_value_type = DataType.STRING) @NotEmpty String anchor,
				@Idx(index = "7", type = TEXT) @Pkg(label = "Offset X" , default_value_type = DataType.STRING) @NotEmpty String anchorX,
				@Idx(index = "8", type = TEXT) @Pkg(label = "Offset Y" , default_value_type = DataType.STRING) @NotEmpty String anchorY) throws Exception 
{

	  	DocuSignSession docuSignSession = (DocuSignSession) this.sessions.get(sessionName);  


       byte[] buffer;
       String docBase64 = null;
		try {
			buffer = readFile(doc);
	         docBase64 = new String(Base64.getEncoder().encode(buffer));
		} catch (IOException e) {
			e.printStackTrace();
		}
     	  

       Document document = new Document();
       document.setDocumentBase64(docBase64);
       document.setName("Sign Document"); // can be different from actual file name
       document.setFileExtension("xlsx"); // many different document types are accepted
       document.setDocumentId("1"); // a label used to reference the doc


       Signer signer = new Signer();
       signer.setEmail(signerEmail);
       signer.setName(signerName);
       signer.recipientId("1");

       SignHere signHere = new SignHere();
       signHere.setDocumentId("1");
       signHere.setPageNumber(String.valueOf(page.intValue()));
       signHere.setRecipientId("1");
       signHere.anchorString(anchor);
       signHere.anchorXOffset(anchorX);
       signHere.anchorYOffset(anchorY);
       signHere.getAnchorIgnoreIfNotPresent();


       Tabs signerTabs = new Tabs();
       signerTabs.setSignHereTabs(Arrays.asList(signHere));
       signer.setTabs(signerTabs);



       EnvelopeDefinition envelopeDefinition = new EnvelopeDefinition();
       envelopeDefinition.setEmailSubject("Please sign this document");
       envelopeDefinition.setDocuments(Arrays.asList(document));

       Recipients recipients = new Recipients();
       recipients.setSigners(Arrays.asList(signer));
       envelopeDefinition.setRecipients(recipients);
       envelopeDefinition.setStatus("sent"); 
 
	   ApiClient apiClient = new ApiClient(docuSignSession.getBasePath()+"/restapi");
	   
	   
       apiClient.addDefaultHeader("Authorization", "Bearer " + docuSignSession.getToken());
       EnvelopesApi envelopesApi = new EnvelopesApi(apiClient);
       EnvelopeSummary results = null;
       String envelopeId ;
	   results = envelopesApi.createEnvelope(docuSignSession.getAccountID(), envelopeDefinition);
	   envelopeId = results.getEnvelopeId();


       return  new StringValue(envelopeId);

   }
	
    private byte[] readFile(String path) throws IOException {
        InputStream is = new FileInputStream(path);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        is.close();
        return buffer.toByteArray();
    }
    
    
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
