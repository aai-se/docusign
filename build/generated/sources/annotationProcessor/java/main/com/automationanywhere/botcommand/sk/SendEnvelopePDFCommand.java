package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class SendEnvelopePDFCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(SendEnvelopePDFCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    SendEnvelopePDF command = new SendEnvelopePDF();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("doc") && parameters.get("doc") != null && parameters.get("doc").get() != null) {
      convertedParameters.put("doc", parameters.get("doc").get());
      if(convertedParameters.get("doc") !=null && !(convertedParameters.get("doc") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","doc", "String", parameters.get("doc").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("doc") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","doc"));
    }

    if(parameters.containsKey("signerName") && parameters.get("signerName") != null && parameters.get("signerName").get() != null) {
      convertedParameters.put("signerName", parameters.get("signerName").get());
      if(convertedParameters.get("signerName") !=null && !(convertedParameters.get("signerName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","signerName", "String", parameters.get("signerName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("signerName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","signerName"));
    }

    if(parameters.containsKey("signerEmail") && parameters.get("signerEmail") != null && parameters.get("signerEmail").get() != null) {
      convertedParameters.put("signerEmail", parameters.get("signerEmail").get());
      if(convertedParameters.get("signerEmail") !=null && !(convertedParameters.get("signerEmail") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","signerEmail", "String", parameters.get("signerEmail").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("signerEmail") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","signerEmail"));
    }

    if(parameters.containsKey("page") && parameters.get("page") != null && parameters.get("page").get() != null) {
      convertedParameters.put("page", parameters.get("page").get());
      if(convertedParameters.get("page") !=null && !(convertedParameters.get("page") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","page", "Number", parameters.get("page").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("page") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","page"));
    }

    if(parameters.containsKey("anchor") && parameters.get("anchor") != null && parameters.get("anchor").get() != null) {
      convertedParameters.put("anchor", parameters.get("anchor").get());
      if(convertedParameters.get("anchor") !=null && !(convertedParameters.get("anchor") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","anchor", "String", parameters.get("anchor").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("anchor") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","anchor"));
    }

    if(parameters.containsKey("anchorX") && parameters.get("anchorX") != null && parameters.get("anchorX").get() != null) {
      convertedParameters.put("anchorX", parameters.get("anchorX").get());
      if(convertedParameters.get("anchorX") !=null && !(convertedParameters.get("anchorX") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","anchorX", "String", parameters.get("anchorX").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("anchorX") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","anchorX"));
    }

    if(parameters.containsKey("anchorY") && parameters.get("anchorY") != null && parameters.get("anchorY").get() != null) {
      convertedParameters.put("anchorY", parameters.get("anchorY").get());
      if(convertedParameters.get("anchorY") !=null && !(convertedParameters.get("anchorY") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","anchorY", "String", parameters.get("anchorY").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("anchorY") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","anchorY"));
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("doc"),(String)convertedParameters.get("signerName"),(String)convertedParameters.get("signerEmail"),(Number)convertedParameters.get("page"),(String)convertedParameters.get("anchor"),(String)convertedParameters.get("anchorX"),(String)convertedParameters.get("anchorY")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
