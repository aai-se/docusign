package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class StartSessionDocuSignCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(StartSessionDocuSignCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    StartSessionDocuSign command = new StartSessionDocuSign();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("basePath") && parameters.get("basePath") != null && parameters.get("basePath").get() != null) {
      convertedParameters.put("basePath", parameters.get("basePath").get());
      if(convertedParameters.get("basePath") !=null && !(convertedParameters.get("basePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","basePath", "String", parameters.get("basePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("basePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","basePath"));
    }

    if(parameters.containsKey("accountID") && parameters.get("accountID") != null && parameters.get("accountID").get() != null) {
      convertedParameters.put("accountID", parameters.get("accountID").get());
      if(convertedParameters.get("accountID") !=null && !(convertedParameters.get("accountID") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","accountID", "SecureString", parameters.get("accountID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("accountID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","accountID"));
    }

    if(parameters.containsKey("clientID") && parameters.get("clientID") != null && parameters.get("clientID").get() != null) {
      convertedParameters.put("clientID", parameters.get("clientID").get());
      if(convertedParameters.get("clientID") !=null && !(convertedParameters.get("clientID") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","clientID", "SecureString", parameters.get("clientID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("clientID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","clientID"));
    }

    if(parameters.containsKey("userID") && parameters.get("userID") != null && parameters.get("userID").get() != null) {
      convertedParameters.put("userID", parameters.get("userID").get());
      if(convertedParameters.get("userID") !=null && !(convertedParameters.get("userID") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","userID", "SecureString", parameters.get("userID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("userID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","userID"));
    }

    if(parameters.containsKey("privateKey") && parameters.get("privateKey") != null && parameters.get("privateKey").get() != null) {
      convertedParameters.put("privateKey", parameters.get("privateKey").get());
      if(convertedParameters.get("privateKey") !=null && !(convertedParameters.get("privateKey") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","privateKey", "SecureString", parameters.get("privateKey").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("privateKey") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","privateKey"));
    }

    command.setSessions(sessionMap);
    try {
      command.start((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("basePath"),(SecureString)convertedParameters.get("accountID"),(SecureString)convertedParameters.get("clientID"),(SecureString)convertedParameters.get("userID"),(SecureString)convertedParameters.get("privateKey"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","start"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
